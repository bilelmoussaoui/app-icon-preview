# Hungarian translation for icon-tool.
# Copyright (C) 2019, 2020 Free Software Foundation, Inc.
# This file is distributed under the same license as the icon-tool package.
#
# Balázs Úr <ur.balazs at fsf dot hu>, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: icon-tool master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/design/app-icon-preview/i"
"ssues\n"
"POT-Creation-Date: 2019-12-29 23:42+0000\n"
"PO-Revision-Date: 2020-02-13 20:10+0100\n"
"Last-Translator: Balázs Úr <ur.balazs at fsf dot hu>\n"
"Language-Team: Hungarian <gnome-hu-list at gnome dot org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 19.04.3\n"

#: data/org.gnome.design.AppIconPreview.metainfo.xml.in.in:8
#: data/org.gnome.design.AppIconPreview.desktop.in.in:3 src/main.vala:118
#: src/saver.vala:60 src/views/colour.vala:97 src/window.ui:13
#: src/window.vala:152 src/window.vala:321
#| msgid "Icon Preview"
msgid "App Icon Preview"
msgstr "Alkalmazásikon-előnézet"

#: data/org.gnome.design.AppIconPreview.metainfo.xml.in.in:9
#| msgid "Tool for designing icons"
msgid "Tool for designing applications icons"
msgstr "Egy eszköz alkalmazások ikonjainak tervezéséhez"

#: data/org.gnome.design.AppIconPreview.metainfo.xml.in.in:11
#| msgid ""
#| "Icon Preview is a tool for designing icons which target the GNOME desktop."
msgid ""
"App Icon Preview is a tool for designing icons which target the GNOME "
"desktop."
msgstr ""
"Az Alkalmazásikon-előnézet olyan ikonok tervezéséhez készült eszköz, amelyek"
" a GNOME asztal számára készülnek."

#: data/org.gnome.design.AppIconPreview.metainfo.xml.in.in:22
msgid "Previewing an Application icon"
msgstr "Egy alkalmazásikon előnézete"

#: data/org.gnome.design.AppIconPreview.metainfo.xml.in.in:54
msgid "Zander Brown"
msgstr "Zander Brown"

#: data/org.gnome.design.AppIconPreview.desktop.in.in:4
#| msgid "Previewing an Application icon"
msgid "Preview applications icons"
msgstr "Alkalmazások ikonjainak előnézete"

#: src/common.vala:36
msgid "Copyright © 2018-19 Zander Brown"
msgstr "Copyright © 2018-19 Zander Brown"

#: src/common.vala:40
msgid "translator-credits"
msgstr "Úr Balázs <ur.balazs at fsf dot hu>, 2019, 2020."

#: src/common.vala:42
msgid "Repository"
msgstr "Tároló"

#: src/common.vala:44
msgid "Kept sane by"
msgstr "Fenntartják az észszerűséget"

#: src/exporter/exporter.ui:18
msgid ""
"Export the icon for production use. Off-canvas objects are removed and the "
"SVG is optimised for size"
msgstr ""
"Az ikon exportálása produktív használathoz. A vásznon kívüli objektumok "
"eltávolításra kerülnek, és az SVG optimalizálva lesz a mérethez"

#: src/exporter/exporter.ui:68
msgid "Regular"
msgstr "Szabályos"

#: src/exporter/exporter.ui:88
msgid "Save Regular As…"
msgstr "Szabályos mentése másként…"

#: src/exporter/exporter.ui:144
msgid "Nightly"
msgstr "Éjszakai"

#: src/exporter/exporter.ui:164
msgid "Save Nightly As…"
msgstr "Éjszakai mentése másként…"

#: src/exporter/exporter.ui:195
msgid "Save Symbolic As…"
msgstr "Szimbolikus mentése másként…"

#: src/exporter/exporter.ui:217
msgid "Symbolic"
msgstr "Szimbolikus"

#: src/help-overlay.ui:13
msgctxt "shortcut window"
msgid "Application"
msgstr "Alkalmazás"

#: src/help-overlay.ui:19
msgctxt "shortcut window"
msgid "New Window"
msgstr "Új ablak"

#: src/help-overlay.ui:26
msgctxt "shortcut window"
msgid "Open an icon"
msgstr "Egy ikon megnyitása"

#: src/help-overlay.ui:33
msgctxt "shortcut window"
msgid "Open the recent list"
msgstr "A legutóbbi lista megnyitása"

#: src/help-overlay.ui:40
msgctxt "shortcut window"
msgid "Open the menu"
msgstr "A menü megnyitása"

#: src/help-overlay.ui:47
msgctxt "shortcut window"
msgid "Quit the application"
msgstr "Kilépés az alkalmazásból"

#: src/help-overlay.ui:54
msgctxt "shortcut window"
msgid "View"
msgstr "Nézet"

#: src/help-overlay.ui:60
msgctxt "shortcut window"
msgid "Reload the icon"
msgstr "Az ikon újratöltése"

#: src/help-overlay.ui:67
msgctxt "shortcut window"
msgid "Export the icon"
msgstr "Az ikon exportálása"

#: src/help-overlay.ui:74
msgctxt "shortcut window"
msgid "Shuffle icons"
msgstr "Ikonok megkeverése"

#: src/help-overlay.ui:81
msgctxt "shortcut window"
msgid "Take screenshot"
msgstr "Képernyőkép készítése"

#: src/help-overlay.ui:88
msgctxt "shortcut window"
msgid "Copy a screenshot to clipboard"
msgstr "Képernyőkép másolása a vágólapra"

#: src/help-overlay.ui:95
msgctxt "shortcut window"
msgid "Toggle fullscreen"
msgstr "Teljes képernyő átváltása"

#: src/main.vala:49
msgid "no longer supported"
msgstr "többé nem támogatott"

#. If opening the palette directly
#: src/main.vala:97
msgid ""
"Palette is all grown up!\n"
"It’s now available separately as org.gnome.zbrown.Palette"
msgstr ""
"A paletta teljesen felnőtt!\n"
"Mostantól különállóan érhető el org.gnome.zbrown.Palette objektumként"

#: src/menus.ui:6
msgid "_New Window"
msgstr "Ú_j ablak"

#: src/menus.ui:12
msgid "_Reload"
msgstr "Új_ratöltés"

#: src/menus.ui:17
msgid "_Take Screenshot"
msgstr "_Képernyőkép készítése"

#: src/menus.ui:22
msgid "_Shuffle Example Icons"
msgstr "_Példaikonok megkeverése"

#: src/menus.ui:29
msgid "_Keyboard Shortcuts"
msgstr "_Gyorsbillentyűk"

#: src/menus.ui:33
#| msgid "_About Icon Preview"
msgid "_About App Icon Preview"
msgstr "Az Alkalmazásikon-előnézet _névjegye"

#: src/saver.ui:50
msgid "Close"
msgstr "Bezárás"

#: src/saver.ui:59
msgid "Save…"
msgstr "Mentés…"

#: src/saver.ui:77
msgid "Copy to Clipboard"
msgstr "Másolás a vágólapra"

#: src/saver.vala:48
msgid "Save Screenshot"
msgstr "Képernyőkép mentése"

#: src/saver.vala:48 src/window.vala:250
msgid "_Save"
msgstr "_Mentés"

#: src/saver.vala:52
msgid "Preview"
msgstr "Előnézet"

#: src/saver.vala:69
msgid "PNG"
msgstr "PNG"

#: src/saver.vala:75
msgid "JPEG"
msgstr "JPEG"

#: src/saver.vala:86
msgid "Failed to save screenshot"
msgstr "A képernyőkép mentése sikertelen"

#: src/window.ui:21
msgid "Open"
msgstr "Megnyitás"

#: src/window.ui:25
msgid "Open an icon"
msgstr "Egy ikon megnyitása"

#: src/window.ui:39
msgid "Recent"
msgstr "Legutóbbi"

#: src/window.ui:67
msgid "Menu"
msgstr "Menü"

#: src/window.ui:83
msgid "Export"
msgstr "Exportálás"

#: src/window.ui:119
#| msgid "Make a new GNOME Icon"
msgid "Make a new App Icon"
msgstr "Új alkalmazásikon készítése"

#: src/window.ui:274
msgid "New App Icon"
msgstr "Új alkalmazásikon"

#: src/window.vala:143
msgid "This file is defective"
msgstr "Ez a fájl hiányos"

#: src/window.vala:144
msgid ""
"Please start from a template to ensure that your file will work as a GNOME "
"icon"
msgstr ""
"Kezdjen egy sablonból, annak biztosításához hogy a fájl működjön GNOME "
"ikonként"

#: src/window.vala:184
msgid "Select Icon"
msgstr "Ikon kiválasztása"

#: src/window.vala:184
msgid "_Open"
msgstr "_Megnyitás"

#: src/window.vala:186
#| msgid "_Cancel"
msgid "Cancel"
msgstr "Mégse"

#: src/window.vala:189
msgid "Icons"
msgstr "Ikonok"

#: src/window.vala:232
#| msgid "Save Regular As…"
msgid "Save Regular"
msgstr "Szabályos mentése"

#: src/window.vala:237
#| msgid "Save Symbolic As…"
msgid "Save Symbolic"
msgstr "Szimbolikus mentése"

#: src/window.vala:243
#| msgid "Save Nightly As…"
msgid "Save Nightly"
msgstr "Éjszakai mentése"

#: src/window.vala:257
msgid "Icon"
msgstr "Ikon"

#: src/window.vala:263
msgid "SVG"
msgstr "SVG"

#: src/window.vala:273
msgid "Failed to save exported file"
msgstr "Nem sikerült elmenteni az exportált fájlt"

#: src/wizard.ui:46
msgid "App Name"
msgstr "Alkalmazásnév"

#: src/wizard.ui:58
msgid "Icon Location"
msgstr "Ikon helye"

#: src/wizard.ui:79
msgid "Save Icon"
msgstr "Ikon mentése"

#: src/wizard.ui:94
msgid "The reverse domain notation name, e.g. org.inkscape.Inkscape"
msgstr "A fordított tartományjelölés neve, például org.inkscape.Inkscape"

#: src/wizard.ui:110
msgid "The icon SVG file will be stored in this directory"
msgstr "Az ikon SVG-fájl ebben a könyvtárban lesz eltárolva"

#: src/wizard.ui:159
msgid "_Cancel"
msgstr "_Mégse"

#: src/wizard.ui:165
msgid "_Create"
msgstr "_Létrehozás"

#: src/wizard.vala:31
msgid "Expecting at least one “.”"
msgstr "Legalább egy „.” elvárt"
